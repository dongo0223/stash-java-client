package com.atlassian.stash.rest.client.api;

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.Map;

public class PropertyMatcher<T> extends BaseMatcher<T> {
    private final Map<String, Matcher<?>> expectedPropertyMatchers;

    public PropertyMatcher(Map<String, Matcher<?>> expectedPropertyMatchers) {
        this.expectedPropertyMatchers = expectedPropertyMatchers;
    }

    private Map<String, PropertyDescriptor> getPropertyDescriptors(Class<?> cls) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(cls);
            return Maps.uniqueIndex(
                    Arrays.asList(beanInfo.getPropertyDescriptors()),
                    new Function<PropertyDescriptor, String>() {
                        @Override
                        public String apply(PropertyDescriptor input) {
                            return input.getName();
                        }
                    }
            );
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean matches(Object o) {
        try {
            if (o == null) {
                return false;
            }
            Map<String, PropertyDescriptor> propertyDescriptors = getPropertyDescriptors(o.getClass());
            for (Map.Entry<String, Matcher<?>> fieldEntry : expectedPropertyMatchers.entrySet()) {
                String name = fieldEntry.getKey();
                Matcher<?> matcher = fieldEntry.getValue();
                Object propertyValue = propertyDescriptors.get(name).getReadMethod().invoke(o);
                if (!matcher.matches(propertyValue)) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public void describeTo(Description description) {
        int i = 0;
        description.appendText("[properties: ");
        for (Map.Entry<String, Matcher<?>> fieldEntry : expectedPropertyMatchers.entrySet()) {
            String name = fieldEntry.getKey();
            Matcher<?> matcher = fieldEntry.getValue();
            description.appendText((i > 0 ? ", " : "") + "'" + name + "' ");
            matcher.describeTo(description);
            i++;
        }
        description.appendText("]");
    }
}