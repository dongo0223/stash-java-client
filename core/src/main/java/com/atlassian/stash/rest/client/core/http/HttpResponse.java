package com.atlassian.stash.rest.client.core.http;

import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class HttpResponse {
    private final int statusCode;
    @Nonnull private final String statusMessage;
    @Nonnull private final Map<String, String> headers;
    @Nonnull private final InputStream bodyStream;

    /**
     * @param statusCode HTTP status code eg. 200, 404
     * @param statusMessage HTTP status message eg. OK, Not Found
     * @param headers Map of HTTP response headers. The header values will include
     *                all values, comma separated, if multiple header fields with the given name were specified, as per RFC2616
     * @param bodyStream Response body input stream
     */
    public HttpResponse(int statusCode, @Nonnull String statusMessage, @Nonnull Map<String, String> headers, @Nonnull InputStream bodyStream) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        // make header names case insensitive
        this.headers = Maps.newTreeMap(String.CASE_INSENSITIVE_ORDER);
        this.headers.putAll(headers);
        this.bodyStream = bodyStream;
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Nonnull
    public String getStatusMessage() {
        return statusMessage;
    }

    @Nonnull
    public InputStream getBodyStream() {
        return bodyStream;
    }

    @Nonnull
    public Reader getBodyReader(@Nullable String defaultEncoding) throws UnsupportedEncodingException {
        String encoding = getContentEncoding(defaultEncoding);
        if (encoding == null) {
            encoding = "ISO-8859-1";
        }
        return new InputStreamReader(getBodyStream(), encoding);
    }

    @Nullable
    public String getContentType() {
        return HttpUtil.getContentType(headers);
    }

    @Nullable
    public String getContentEncoding(@Nullable String defaultEncoding) {
        return HttpUtil.getContentEncoding(headers, defaultEncoding);
    }

    /**
     * Map of HTTP response headers. The key lookups will behave in case-insensitive manner. The header values will include
     * all values, comma separated, if multiple header fields with the given name were specified, as per RFC2616
     */
    @Nonnull
    public Map<String, String> getHeaders() {
        return headers;
    }

    public boolean isSuccessful() {
        return statusCode >= 200 && statusCode < 400;
    }
}
